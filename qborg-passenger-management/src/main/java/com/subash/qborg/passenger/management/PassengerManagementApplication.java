package com.subash.qborg.passenger.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PassengerManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassengerManagementApplication.class, args);
	}

}
