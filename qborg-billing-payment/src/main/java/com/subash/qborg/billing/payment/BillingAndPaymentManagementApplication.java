package com.subash.qborg.billing.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillingAndPaymentManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillingAndPaymentManagementApplication.class, args);
	}

}
